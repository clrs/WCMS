<?php

class YaoController extends Action
{

    static $service;

    public function meeting ()
    {
        $this->view()->display("file:plugin/meeting.tpl");
    }

    public function start ()
    {
        self::getService()->cleanList();
    }

    public function sync ()
    {
        $rs = self::getService()->getUserMaxId($_POST['maxid']);
        $this->sendNotice($rs['message'], $rs['data'], $rs['status']);
    }

    public function us ()
    {
        $weixin = new WeixinService();
        $rs = $weixin->getOpenIdByCode($_GET['code']);
        if (! $rs['status']) {
            echo $rs['message'];
            exit();
        }
        
        $this->view()->assign('openid', $rs['data']);
        
        $this->view()->display("file:plugin/yao.tpl");
    }

    public function yao ()
    {
        $rs = self::getService()->add($_POST['openid']);
        $this->sendNotice($rs['message'], $rs['data'], $rs['status']);
    }

    public static function getService ()
    {
        if (self::$service == null) {
            self::$service = new YaoService();
        }
        return self::$service;
    }
}