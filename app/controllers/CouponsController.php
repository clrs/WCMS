<?php

class CouponsController extends NodeController
{

    static $couponsService;

    static $memberService;

    /**
     * 审计列表
     */
    public function audit ()
    {
        // 统计当前用户总账
        $rs = self::getCouponsService()->audit();
        // 获取审计账目 最近30条
        $audit = self::getCouponsService()->getAudit(30);
        $this->view()->assign("coupons", $rs);
        $this->view()->assign('audit', $audit);
        $this->view()->display('file:coupons/audit.tpl');
    }

    /**
     * 提交审计
     * Enter description here .
     * ..
     */
    public function setAudit ()
    {
        $rs = self::getCouponsService()->setAudit($_POST);
        
        $this->sendNotice($rs);
    }

    /**
     * 导出明细
     */
    public function export ()
    {
        self::getCouponsService()->setCsvHeader();
        /* 按照时间到处 */
        if (isset($_POST['uid']) && $_POST['uid'] > 0) {
            $where = array(
                'status' => 8,
                'uid' => $_POST['uid']
            );
            $userinfo = MemberModel::instance()->getOneMember(array(
                'uid' => $_POST['uid']
            ));
        } else {
            $where = array(
                'status' => 8
            );
            $userinfo['real_name'] = "全部";
        }
        
        self::getCouponsService()->export($_POST['starttime'], $_POST['endtime'], $where);
    }
    
    // 充值明细 全部的数据
    public function listing ()
    {
        // 默认设置为15条
        $rs = self::getCouponsService()->listing($_GET['p'], null);
        $this->view()->assign('num', $rs['page']);
        $this->view()->assign('totalnum', $rs['totalnum']);
        
        $lastMoth = strtotime("-1 month");
        $this->view()->assign("time", array(
            'lastmonth' => date("Y.m.d", $lastMoth),
            'now' => date("Y.m.d", time())
        ));
        $this->view()->assign("mx", $rs['list']);
        $this->view()->display("file:coupons/list.tpl");
    }
    /*
     * 后台用户积分明细
     */
    public function user ()
    {
        $rs = self::getCouponsService()->listing($_GET['p'], array(
            'uid' => $_GET['uid']
        ));
        $user = self::getMemberService()->getMemberByUid($_GET['uid']);
        $total = self::getCouponsService()->getSumCouponsByUid($_GET[uid]);
        $this->view()->assign("userinfo", $user);
        $this->view()->assign("num", $rs['page']);
        $this->view()->assign('total', $total['total']);
        $this->view()->assign("uid", $_GET['uid']);
        $this->view()->assign("mx", $rs['list']);
        $this->view()->display("file:coupons/mx.tpl");
    }

    /**
     * 导入代码
     * Enter description here .
     * ..
     */
    public function import ()
    {
        echo self::getCouponsService()->import(self::getMemberService(), $_POST['remark']);
    }

    /**
     * 增加积分
     */
    public function addCoupons ()
    {
        $uid = $_POST['uid'];
        $coupons = trim($_POST['coupons']);
        $remark = trim($_POST['remark']);
        $user = self::getMemberService()->getMemberByUid($uid);
        
        $id = self::getCouponsService()->addCoupons($uid, $user['real_name'], $coupons, $remark);
        if ($id > 0) {
            
            self::getMemberService()->saveCoupons($uid, $coupons, 0);
        } else {
            $this->sendNotice(self::ERROR);
        }
        $this->sendNotice(self::SUCEESS);
    }
    /*
     * 导入积分
     */
    public function csv ()
    {
        $this->view()->display("file:coupons/csv.tpl");
    }

    public function verify ()
    {
        self::getCouponsService()->verify();
    }

    /**
     * 用户前端查询
     */
    public function coupons ()
    {
        $user = $this->_user_global;
        $rs = self::getCouponsService()->listing($_GET['p'], array(
            'uid' => $user['uid']
        ));
        $this->view()->assign("mx", $rs['list']);
        $this->view()->assign('userinfo', $user);
        $this->view()->assign('totalnum', $rs['totalnum']);
        $this->view()->assign('num', $rs['page']);
        $this->view()->display("file:coupons/coupons.tpl");
    }

    public static function getMemberService ()
    {
        if (self::$memberService == null) {
            self::$memberService = new MemberService();
        }
        return self::$memberService;
    }

    public static function getCouponsService ()
    {
        if (self::$couponsService == null) {
            self::$couponsService = new CouponsService();
        }
        return self::$couponsService;
    }
}