{include file="news/header.tpl"} {include file="module/add.header.tpl"}

<tr>
	<td></td>
	<td colspan="2">
	<!-- 加载编辑器的容器 -->
    <script id="container" name="content" type="text/plain">
    </script>
	</td>
	<td></td>

</tr>

<tr>
	<td>摘要</td>
	<td><textarea cols="80" id="summary" name="summary" rows="5"
		style="width: 700px;" class="form-control"></textarea></td>
	<td></td>

</tr>



<tr>
	<td>作者</td>
	<td><input type="text" name="author" value="{$user.username}"  class="form-control"></td>
	<td></td>

</tr>
<tr>
	<td>关键词</td>
	<td>
	<input type="text" name="keyword"  class="form-control">
	
	
	<td></td>

</tr>

<tr>
	<td>模板</td>
	<td><input type="text" name="template" class="input-xlarge" value=""></td>
	<td></td>

</tr>
<tr>
	<td></td>
	<td colspan=2 class=btnline align=center><input type="hidden" name="id"
		value="{$news.id}">
	<button type="submit" class="btn "><i class="icon-ok"></i>{$lang['CREATE']}
	</button>
	<input type="reset" value="{$lang['RESET']}" class=btn disabled></td>
	<td></td>
</tr>

<tr>
	<td></td>
	<td colspan=2 class=btnline align=center></td>
	<td></td>
</tr>

</table>
</div>
<div class="tab-pane" id="tab2">
<table class="table">


	
	<tr>
		<td class="span2">权重</td>
		<td><input type="text" name="sort" value="0" class="input-small"></td>

		<td></td>

	</tr>
	<tr>
		<td>浏览次数</td>
		<td><input type="text" name="views" value="1"  class="input-small"></td>

		<td></td>

	</tr>
	<tr>
		<td>权限</td>
		<td><select name="groupid" class="form-control">
			{foreach from=$group item=l key=key}

			<option value={$key} {if $key==0}selected{/if}>{$l}</option>
			{/foreach}

		</select></td>
		<td></td>

	</tr>

	<tr>
		<td>发布时间</td>
		<td>
		<span class="input-append date" id="form_date" data-date-format="yyyy-mm-dd hh:ii">
		
		<input type="text" name="date" value="{$smarty.now|date_format:'%Y-%m-%d %H:%M'}" class=" uneditable-input">
		
    <span class="add-on"><i class="icon-calendar"></i></span></span>
		</td>

		<td></td>

	</tr>

	<tr>
		<td>状态</td>
		<td><input type="radio" name="status" value="0"
			{if $news.status==0}checked{/if}>{$lang['SHOW']} &nbsp;&nbsp;<input
			type="radio" name="status" value="1" {if $news.status==1}checked{/if}>{$lang['HIDDEN']}

		</td>

		<td></td>

	</tr>

	


	<tr id="submit">
		<td><input type="hidden" name="mid" value="{$module}"></td>
		<td>
		<button type="submit" class="btn "><i class="icon-ok"></i>{$lang['CREATE']}
		</button>
		<input type="reset" value="{$lang['RESET']}" class=btn disabled></td>
		<td></td>

	</tr>
</table>
</div>

<div class="tab-pane" id="tab3">

<table class="table" id="extend">
{foreach from=$extend item=l}
	<tr>
		{if $l.status==3}
		<td><input type="button" class="btn btn-primary"
			onclick="apd('{$l.key}');" value="增加">{$l.name}</td>

		<td id="{$l.key}">{section name=loop loop=$l.num} <input type="text"
			name="{$l.key}[]" class="form-control" value="0"> {/section} {else}
		
		
		<td>{$l.name}</td>

		<td>
		<div class="form-group col-md-5"><input type="text"
			class="form-control" name="{$l.key}">{$l.key}({$l.type})</div>
		{/if}</td>
		<td></td>

	</tr>
	

	{/foreach}
	<tr>
		<td><input type="hidden" name="mid" value="{$module}"></td>
		<td>
		<button type="submit" class="btn "><i class="icon-ok"></i>{$lang['CREATE']}
		</button>
		<input type="reset" value="重置" class=btn disabled></td>
		<td></td>

	</tr>
</table>
</div>

</form>
</div>
</div>
</div>
</div>
{include file="module/footer.tpl"}

{include file="news/footer.tpl"}
